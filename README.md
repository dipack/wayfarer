# Dipack P Panjabi's CSE 586 Project 1
## 1. Overview
This project has been created and is maintained by Dipack P Panjabi, UB person number: 50291077,
for CSE 586, Fall semester, 2018. This project is intended to showcase the capabilities of a simple
distributed system. Its main purpose is to display driving directions from one point to another,
and also display weather along the route, at periodic intervals

## 2. Setup
### 2.1. Database  
- Move into the `wayfarer-db` folder  
- Ensure you have the `mongo*` packages installed on your system, and on `$PATH`  
- Run the script `startup-mongod.sh`

### 2.2. Services  
- Move into the `wayfarer-services/src` folder  
- Ensure you have the following python packages installed `flask pymongo bson`, and the `flask` executable is in `$PATH`  
- Run the app using the command `flask run`  
NOTE: You need to have followed `Section 2.4`

### 2.3. Front end  
#### Option A - Run pre-built version
- Simply serve the following folder using an HTTP server of your choice `wayfarer-ui/dist/wayfarer-ui/`
- An example would be: `cd wayfarer-ui/dist/wayfarer-ui/; python3 -m http.server 9091`
- Then access the website at `localhost:9091`, assuming the database and services are switched on  
NOTE: You need to have followed `Section 2.4`
#### Option B - Locally build the front-end application
- Ensure that you have `node>=8.11.4` and `npm>=5.6.0` installed, and on `$PATH`
- Install `yarn>=1.9.4` globally by running, `npm install -g yarn@1.9.4`  
- Move into the `wayfarer-ui` folder
- Install the dependencies by running `yarn install`  
- Once the installation is complete, you can start the `express.js` server, 
and access the app at `localhost:4200`, by running the command `./node_modules/.bin/ng serve`  
NOTE: You need to have followed `Section 2.4`
- Optionally, you can build the application after the dependencies are done installing, by running `./node_modules/.bin/ng build --prod`, and following `Option A`

### 2.4. API keys  
- Finally, to get the APIs working you will have to generate a key for each one of Mapbox and OpenWeatherMap  
- Place both these keys in two locations: `wayfarer-services/src/apiKeys.py` and `wayfarer-ui/src/app/api-keys.ts` 
