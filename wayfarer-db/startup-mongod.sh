#!/usr/bin/env bash
BIN="$(which mongod)"
DBPATH="./data/db";
LOG_DIR="./logs";
LOGPATH="$LOG_DIR/mongo.log";
if [ ! -d "$DBPATH" ]; then
    mkdir -p "$DBPATH";
fi
if [ ! -f "$LOG_DIR" ]; then
    mkdir -p "$LOG_DIR";
fi
"$BIN" --dbpath "$DBPATH" --logpath "$LOGPATH" --logappend --journal
