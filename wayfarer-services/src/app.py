import datetime
import requests as r
import pymongo as mongo
from bson import json_util
from flask import Flask, request, make_response, render_template, Response, jsonify
import apiKeys

app = Flask(__name__)


# TODO: Objectives for server
# 1. Drop Origin and Destination into the DB
# 2. Also drop the route data into the DB
# 3. Call OWM from the server by proxy
# 4. Try to cache the images as well on server side

class OpenWeatherMap:
    def __init__(self):
        return

    @staticmethod
    def fetch_weather_icon(icon_url_bit="10d"):
        if icon_url_bit.startswith("http") or icon_url_bit.endswith("png"):
            return icon_url_bit
        else:
            return "http://openweathermap.org/img/w/{}.png".format(icon_url_bit)


def init_db_connection():
    client = mongo.MongoClient("localhost", 27017)
    return client['wayfarer']


@app.route("/fetch-weather", methods=['GET'])
def fetch_weather():
    lat = float(request.args.get('lat'))
    lng = float(request.args.get('lng'))
    url = "https://api.openweathermap.org/data/2.5/weather?lat={0}&lon={1}&appid={2}".format(
        round(lat, 2), round(lng, 2), apiKeys.OpenWeatherMap
    )
    response = r.get(url)
    return Response(response.content, mimetype='application/json')


@app.route("/reverse-geocode", methods=['GET'])
def reverse_geocode():
    lat = float(request.args.get('lat'))
    lng = float(request.args.get('lng'))
    url = "https://api.mapbox.com/geocoding/v5/mapbox.places/{0}%2C{1}.json?access_token={2}&types=place&limit=1" \
        .format(round(lng, 2), round(lat, 2), apiKeys.Mapbox)
    response = r.get(url)
    return Response(response.content, mimetype='application/json')


@app.route("/fetch-routes", methods=["GET"])
def fetch_all_routes():
    db = init_db_connection()
    routes = list(db.get_collection('HistoricalRoutes').find().sort([('timestamp', mongo.DESCENDING)]))
    return Response(json_util.dumps(routes), mimetype='application/json')


@app.route("/store-route", methods=["POST"])
def store_user_route():
    db = init_db_connection()
    route = request.get_json()['route']
    route['timestamp'] = datetime.datetime.utcnow().isoformat()
    # Insert into mongo with appropriate enrichment
    db.get_collection('HistoricalRoutes').insert(route)
    return ''


@app.route("/compute-directions/<source>/<mode>/<fullpath>", methods=['GET'])
def compute_directions(source, mode, fullpath):
    query_params = "".join(["{0}={1}&".format(k, v) for k, v in request.args.items()])
    url = "https://api.mapbox.com/directions/v5/{0}/{1}/{2}?{3}".format(source, mode, fullpath, query_params)
    response = r.get(url)
    return Response(response=response, mimetype='application/json')


@app.after_request
def allow_cross_domain(response: Response):
    """Hook to set up response headers."""
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'content-type'
    return response


if __name__ == '__main__':
    app.run()
