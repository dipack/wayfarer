import { LngLat } from 'mapbox-gl';
import * as moment from 'moment';

export interface MapboxRoute {
  route: SimpleRoute[];
}

export interface SimpleRoute {
  distance: number; // in meters
  duration: number; // seconds
  geometry: string;
  legs: SimpleRouteLeg[];
  weight: number;
  weight_name: string;
}

interface SimpleRouteLeg {
  distance: number; // in meters
  duration: number; // seconds
  steps: SimpleRouteLegStep[];
  weight: number;
  summary: string;
}

export interface SimpleRouteLegStep {
  distance: number; // in meters
  driving_side: string;
  duration: number; // seconds
  geometry: string;
  intersections: any[];
  maneuver: SimpleRouteLegStepManeuver;
  mode: string;
  name: string;
  weight: number;
}

export interface SimpleRouteLegStepManeuver {
  location: number[]; // [lng, lat]
  instruction: string;
}

// OpenWeatherMap Weather API response structure
// https://openweathermap.org/current
export interface OWMWeatherResponse {
  coord: { lon: number, lat: number };
  weather: Array<{ id: number, main: string, description: string, icon: string }>;
  main: { temp: number, humidity: number, pressure: number, temp_min: number, temp_max: number };
  wind: { speed: number, deg: number };
  rain?: { '3h': number };
  snow?: { '3h': number };
  clouds: { all: number };
}

export interface WeatherAt {
  // Random ID
  id?: string;
  coordinates: LngLat;
  placeName: string;
  weather: {
    iconUrl?: string,
    condition: string,
    description: string,
    temp: { current: number, max: number, min: number }
  };
}

export class RouteForDB {
  constructor(public origin: LngLat = LngLat.convert([0, 0]),
              public destination: LngLat = LngLat.convert([0, 0]),
              public route: SimpleRoute,
              public timestamp: string = moment.utc().toISOString(),
              public weatherAt: WeatherAt[] = []) {
    this.origin = LngLat.convert(origin);
    this.destination = LngLat.convert(destination);
  }

  static constructFromJson(json: any) {
    return new RouteForDB(json.origin, json.destination, json.route, json.timestamp, json.weatherAt);
  }
}
