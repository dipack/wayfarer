import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MapperiumComponent } from './mapperium.component';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { APIKeys } from '../api-keys';
import { MapDirectionsComponent } from './map-directions.component';
import { MapperiumService } from './mapperium.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    NgxMapboxGLModule.withConfig({
      accessToken: APIKeys.Mapbox
    })
  ],
  declarations: [
    MapperiumComponent,
    MapDirectionsComponent
  ],
  entryComponents: [
    MapperiumComponent,
    MapDirectionsComponent
  ],
  providers: [
    MapperiumService
  ],
  exports: [
    MapperiumComponent,
    MapDirectionsComponent
  ]
})
export class MapModule {
}
