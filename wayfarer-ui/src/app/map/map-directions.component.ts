import { Component, Input } from '@angular/core';
import { WeatherAt } from '../declarations';

@Component({
  selector: 'mapperium-directions',
  template: `
    <div class="row wayfarer-directions-container">
      <div class="row">
        <div class="col-md-12 route-weather-list"
             *ngFor="let weather of weatherList; trackBy: trackByWeatherAt ; let idx = index;">
          <h3>Waypoint {{ idx + 1 }} - {{ weather.placeName }}</h3>
          <div class="row">
            <p class="col-md-8">The weather at {{ weather.placeName }} is: <br>
              {{ weather.weather.condition | titlecase }} ({{ weather.weather.description | titlecase }}) <br>
              with highs of {{ (weather.weather.temp.max - 273).toFixed(1) }} Celsius and lows of
              {{ (weather.weather.temp.min - 273).toFixed(1) }} Celsius
            </p>
            <div class="col-md-4">
              <img class="directions-weather-icon" *ngIf="weather.weather.iconUrl" src="{{ weather.weather.iconUrl }}">
            </div>
          </div>
        </div>
      </div>
    </div>
  `
})
export class MapDirectionsComponent {
  @Input() public weatherList: WeatherAt[];

  constructor() {
  }

  trackByWeatherAt(idx: number, weatherAt: WeatherAt) {
    return weatherAt.id;
  }
}
