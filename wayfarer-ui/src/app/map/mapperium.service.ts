import * as moment from 'moment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RouteForDB } from '../declarations';
import { LngLat } from 'mapbox-gl';
import { Mapbox, MapboxGeocodingResponse } from '../utils/mapbox';
import { WayfarerUtil } from '../utils/wayfarer';
import { OpenWeatherMap } from '../utils/openWeatherMap';

const mockWeatherAtPointResponse = {
  'coord': {'lon': 139, 'lat': 35},
  'sys': {'country': 'JP', 'sunrise': 1369769524, 'sunset': 1369821049},
  'weather': [{'id': 804, 'main': 'clouds', 'description': 'overcast clouds', 'icon': '04n'}],
  'main': {'temp': 289.5, 'humidity': 89, 'pressure': 1013, 'temp_min': 287.04, 'temp_max': 292.04},
  'wind': {'speed': 7.31, 'deg': 187.002},
  'rain': {'3h': 0},
  'clouds': {'all': 92},
  'dt': 1369824698,
  'id': 1851632,
  'name': 'Shuzenji',
  'cod': 200
};

@Injectable()
export class MapperiumService {
  constructor(readonly http: HttpClient) {

  }

  fetchAllRoutes(): Promise<RouteForDB[]> {
    return this.http.get(WayfarerUtil.fetchAllRoutesUrl())
      .toPromise()
      .then((response: any[]) => {
        return response.map((r) => RouteForDB.constructFromJson(r));
      });
  }

  isRouteStale(route: RouteForDB): boolean {
    const currentTime = moment.utc();
    const routeTime = moment(route.timestamp);
    if (moment.duration(currentTime.diff(routeTime)).asHours() > 6) {
      console.warn('Route data is stale; Discarding...');
      return true;
    } else {
      return false;
    }
  }

  findMatchingRoute(dbRoutes: RouteForDB[], source: LngLat, sink: LngLat): RouteForDB {
    let foundRoute: RouteForDB;
    dbRoutes.forEach((dbRoute: RouteForDB) => {
      if (dbRoute.origin.lng === source.lng &&
        dbRoute.origin.lat === source.lat &&
        dbRoute.destination.lng === sink.lng &&
        dbRoute.destination.lat === sink.lat) {
        if (this.isRouteStale(dbRoute) === false) {
          foundRoute = dbRoute;
          return false;
        }
      }
    });
    if (foundRoute) {
      return foundRoute;
    } else {
      return undefined;
    }
  }

  pushRouteToDB(route: RouteForDB): Promise<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(
      'http://localhost:5000/store-route',
      {
        route
      },
      {
        headers
      })
      .toPromise();
  }

  // TODO: Make fewer weather calls, i.e. remove the place and coordinate duplicates, and then call OWM
  getWeather(coordinates: LngLat): Promise<any> {
    // return this.http.get(OpenWeatherMap.constructWeatherFetchURL(coordinates)).toPromise();
    return Promise.resolve(mockWeatherAtPointResponse);
  }

  getPlacename(coordinates: LngLat): Promise<MapboxGeocodingResponse> {
    return this.http.get<MapboxGeocodingResponse>(Mapbox.constructGeocodingURL(coordinates)).toPromise();
  }
}
