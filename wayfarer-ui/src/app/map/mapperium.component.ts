import * as MapboxDirections from '@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions';
import * as turf from '@turf/turf';
import * as uuid from 'uuid/v4';
import * as $ from 'jquery';
import { ChangeDetectorRef, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { GeolocateControl, LngLat, Map, Marker, NavigationControl } from 'mapbox-gl';
import { MapboxRoute, OWMWeatherResponse, RouteForDB, SimpleRoute, SimpleRouteLegStep, WeatherAt } from '../declarations';
import { OpenWeatherMap } from '../utils/openWeatherMap';
import { APIKeys } from '../api-keys';
import { MapboxGeocodingResponse } from '../utils/mapbox';
import { chain, isEqual, uniqWith, cloneDeep } from 'lodash';
import { MapperiumService } from './mapperium.service';
import * as moment from 'moment';
import { WayfarerUtil } from '../utils/wayfarer';

const MAPBOX_DIRECTIONS_CLASS = '.mapboxgl-ctrl-directions';
const MAPBOX_DIRECTIONS_BOX_ID = 'route-directions-box';

@Component({
  // tslint:disable-next-line
  selector: 'mapperium',
  template: `
    <div class="row wayfarer-map-container">
      <div id="map-box" class="col-md-8">
        <mgl-map #mapbox
                 style="height: 100%;"
                 [style]="'mapbox://styles/mapbox/dark-v9'"
                 [interactive]="true"
                 [zoom]="9"
                 [center]="[-75, 40]"
                 (load)="onMapLoad($event)"
        >
        </mgl-map>
      </div>
      <div id="${MAPBOX_DIRECTIONS_BOX_ID}"
           class="col-md-4"
           style="top: 10px; right: 0px;"></div>
    </div>
  `
})
export class MapperiumComponent implements OnInit {
  @Output() onMapUpdate: EventEmitter<WeatherAt[]> = new EventEmitter<WeatherAt[]>();
  @Output() onFetchPastRoutes: EventEmitter<RouteForDB[]> = new EventEmitter<RouteForDB[]>();
  @Output() onToggleSpinner: EventEmitter<boolean> = new EventEmitter<boolean>();

  private map: Map;
  private navigationControl: NavigationControl;
  private geolocationControl: GeolocateControl;
  private directionsControl: MapboxDirections;

  public originCoordinates: LngLat;
  public destinationCoordinates: LngLat;
  private computedRoute: MapboxRoute;

  public weatherList: WeatherAt[];
  public pastRoutes: RouteForDB[];

  constructor(readonly changeDetectorRef: ChangeDetectorRef,
              readonly mapperiumService: MapperiumService) {
  }

  ngOnInit() {
    this.mapperiumService.fetchAllRoutes()
      .then((response: RouteForDB[]) => {
        this.pastRoutes = response;
        this.onFetchPastRoutes.emit(this.pastRoutes);
      });
  }

  trackByWeatherAt(idx: number, weatherAt: WeatherAt) {
    return weatherAt.id;
  }

  onMapLoad(map: Map) {
    this.map = map;
    if (this.map) {
      this.navigationControl = new NavigationControl({showCompass: true, showZoom: true});
      this.geolocationControl = new GeolocateControl({
        showUserLocation: true,
        trackUserLocation: true
      });
      this.directionsControl = new MapboxDirections({
        accessToken: APIKeys.Mapbox,
        api: WayfarerUtil.computeDirectionsUrl(),
        container: 'map-box-directions',
        unit: 'metric',
        profile: 'mapbox/driving'
      });
      this.map.addControl(this.navigationControl);
      this.map.addControl(this.geolocationControl);
      this.map.addControl(this.directionsControl, 'top-left');
      $(MAPBOX_DIRECTIONS_CLASS).appendTo(`#${MAPBOX_DIRECTIONS_BOX_ID}`);
      this.directionsControl.on('route', (e) => this.onRouteChange(e));
    }
  }

  addWeatherDataLayer(weatherList: WeatherAt[]) {
    weatherList.forEach((weatherAt: WeatherAt, idx: number) => {
      if (idx === 0) {
        const el = window.document.createElement('img');
        el.src = weatherAt.weather.iconUrl;
        el.className = 'wayfarer-weather-icon';
        new Marker(el).setLngLat(weatherAt.coordinates).addTo(this.map);
      }
    });
  }

  onRouteChange(route: MapboxRoute) {
    this.onToggleSpinner.emit(true);
    let usingCachedData: boolean = false;
    this.computedRoute = route;

    const origin: number[] = this.directionsControl.getOrigin().geometry.coordinates;
    this.originCoordinates = LngLat.convert(origin);

    const destination: number[] = this.directionsControl.getDestination().geometry.coordinates;
    this.destinationCoordinates = LngLat.convert(destination);

    let chunkedRoute;
    const foundRoute = this.mapperiumService.findMatchingRoute(this.pastRoutes, this.originCoordinates, this.destinationCoordinates);
    if (foundRoute) {
      chunkedRoute = this.chunkRoute(foundRoute.route);
      usingCachedData = true;
    } else {
      chunkedRoute = this.chunkRoute(this.computedRoute.route[0]);
    }

    let weatherPromise: Promise<WeatherAt[]>;

    if (foundRoute && foundRoute.weatherAt.length > 0) {
      // No need to worry about weather data being stale, as MapperiumService.findMatchingRoute() takes
      // care of that for us
      console.warn('Found cached weather data', foundRoute);
      usingCachedData = true;
      weatherPromise = Promise.resolve(foundRoute.weatherAt);
    } else {
      // Setting this instance to false, as if we have the route cached, but not the weather along it
      // we then treat it as if it were a new route
      usingCachedData = false;
      weatherPromise = this.formatWeatherForUI(chunkedRoute);
    }

    const pushToDb: RouteForDB = RouteForDB.constructFromJson({
      origin: this.originCoordinates,
      destination: this.destinationCoordinates,
      route: this.computedRoute.route[0],
      timestamp: moment.utc().toISOString()
    });

    weatherPromise
      .then((weatherList: WeatherAt[]) => {
        this.weatherList = weatherList;
        return weatherList;
      })
      .then((weatherList: WeatherAt[]) => {
        this.addWeatherDataLayer(weatherList);
        this.changeDetectorRef.detectChanges();
      })
      .then(() => {
        this.onMapUpdate.emit(this.weatherList);
      })
      .then(() => {
        if (!usingCachedData) {
          console.log('Not using cached data, so adding to DB');
          pushToDb.weatherAt = cloneDeep(this.weatherList);
          return this.mapperiumService.pushRouteToDB(pushToDb);
        }
      })
      // Pushing current route to DB, as it has been successfully put into the DB
      .then(() => {
        if (!usingCachedData) {
          this.pastRoutes.push(pushToDb);
        }
        this.onToggleSpinner.emit(false);
      })
      .catch((error) => {
        this.onToggleSpinner.emit(false);
        console.error(error);
      });
  }

  getRouteLine(route: SimpleRoute) {
    return route.legs[0].steps.map((step: SimpleRouteLegStep) => {
      return step.maneuver.location;
    });
  }

  chunkRoute(route: SimpleRoute): LngLat[] {
    const routeLine = this.getRouteLine(route);
    const turfRouteLine = turf.lineString(routeLine);
    const sampleDistance = 100000; // in meters
    const numOfPoints = route.distance / sampleDistance;
    const chunkedRouteLine = [];
    let currentDistance = 0;

    chunkedRouteLine.push(LngLat.convert(turf.along(turfRouteLine, 0, {units: 'meters'}).geometry.coordinates));
    for (let point = 0; point < numOfPoints; point++) {
      currentDistance = (point + 1) * sampleDistance;
      const remainingDistance = route.distance - currentDistance;
      if (remainingDistance > 0) {
        const newWayPoint = turf.along(turfRouteLine, currentDistance, {units: 'meters'});
        chunkedRouteLine.push(LngLat.convert(newWayPoint.geometry.coordinates));
      }
    }
    chunkedRouteLine.push(LngLat.convert(turf.along(turfRouteLine, route.distance, {units: 'meters'}).geometry.coordinates));
    // Performing uniq op to avoid dupes
    return uniqWith(chunkedRouteLine, isEqual);
  }

  formatWeatherForUI(chunkedRoute: LngLat[]): Promise<any> {
    const weatherPromises = chunkedRoute.map((point: LngLat) => this.mapperiumService.getWeather(point));
    const placenamePromises = chunkedRoute.map((point: LngLat) => this.mapperiumService.getPlacename(point));
    let weathers: OWMWeatherResponse[] = [];
    let places: MapboxGeocodingResponse[] = [];
    return Promise.all(weatherPromises)
      .then((w: OWMWeatherResponse[]) => {
        weathers = w;
        return Promise.all(placenamePromises);
      })
      .then((p: MapboxGeocodingResponse[]) => {
        places = p;
        const weatherList = weathers.map((weather: OWMWeatherResponse, idx: number) => {
          const placeName = places[idx].features[0] ? places[idx].features[0].place_name : '';
          return {
            id: uuid(),
            coordinates: new LngLat(weather.coord.lon, weather.coord.lat),
            placeName: placeName || '',
            weather: {
              iconUrl: OpenWeatherMap.constructOWMIconUrl(weather.weather[0].icon),
              condition: weather.weather[0].main,
              description: weather.weather[0].description,
              temp: {
                current: weather.main.temp,
                max: weather.main.temp_max,
                min: weather.main.temp_min
              }
            }
          };
        });
        // Performing uniq op to avoid dupes
        // and filtering out entries with empty place name
        const uniqWeatherList = chain(weatherList)
          .uniqBy('placeName')
          .filter((w: WeatherAt) => w.placeName !== '')
          .value();
        return Promise.resolve(uniqWeatherList);
      });
  }

}
