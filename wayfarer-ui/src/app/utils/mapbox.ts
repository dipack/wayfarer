import { LngLat, LngLatLike } from 'mapbox-gl';
import { WayfarerUtil } from './wayfarer';

export interface MapboxGeocodingResponse {
  features: MapboxGeocodingResponseFeature[];
}

export interface MapboxGeocodingResponseFeature {
  place_name: string;
  text: string;
  geometry: { type: string, coordinates: LngLatLike };
}

export class Mapbox {
  static constructGeocodingURL(coordinates: LngLat): string {
    return WayfarerUtil.reverseGeocodeUrl(coordinates);
  }
}
