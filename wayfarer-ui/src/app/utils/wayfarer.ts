import { LngLat } from 'mapbox-gl';

export class WayfarerUtil {
  static baseUrl: string = 'http://localhost:5000';

  static fetchAllRoutesUrl(): string {
    return `${WayfarerUtil.baseUrl}/fetch-routes`;
  }

  static reverseGeocodeUrl(coordinates: LngLat): string {
    return `${WayfarerUtil.baseUrl}/reverse-geocode?lng=${coordinates.lng}&lat=${coordinates.lat}`;
  }

  static fetchWeatherUrl(coordinates: LngLat): string {
    return `${WayfarerUtil.baseUrl}/fetch-weather?lng=${coordinates.lng}&lat=${coordinates.lat}`;
  }

  static computeDirectionsUrl(): string {
    return `${WayfarerUtil.baseUrl}/compute-directions/`;
  }
}
