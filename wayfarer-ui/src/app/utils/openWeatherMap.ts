import { WayfarerUtil } from './wayfarer';
import { LngLat } from 'mapbox-gl';

export class OpenWeatherMap {
  static constructWeatherFetchURL(coordinates: LngLat) {
    return WayfarerUtil.fetchWeatherUrl(coordinates);
  }

  static constructOWMIconUrl(icon: string) {
    if (icon.startsWith('http') || icon.endsWith('png')) {
      return icon;
    } else if (!icon) {
      return `http://openweathermap.org/img/w/10d.png`;
    } else {
      return `http://openweathermap.org/img/w/${icon}.png`;
    }
  }
}
