import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PastRoutesComponent } from './past-routes.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PastRoutesComponent
  ],
  entryComponents: [
    PastRoutesComponent
  ],
  exports: [
    PastRoutesComponent
  ]
})
export class RouteModule {
}
