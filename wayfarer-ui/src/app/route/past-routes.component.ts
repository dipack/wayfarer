import * as moment from 'moment';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { MapperiumService } from '../map/mapperium.service';
import { RouteForDB } from '../declarations';
import { zip } from 'lodash';

@Component({
  selector: 'past-routes',
  template: `
    <div *ngIf="pastRoutes.length === 0">
      <h4>You have no routes! :(</h4>
      <h4>Travel some more!</h4>
    </div>
    <div *ngIf="pastRoutes.length">
      <div *ngFor="let route of pastRoutes; let idx = index;">
        <ng-container *ngIf="namePairs[idx]">
          <br>
          <h3>On {{ formatRouteDate(route.timestamp) }},</h3>
          <h4>From {{ namePairs[idx][0] }} to {{ namePairs[idx][1] }}</h4>
          <hr>
        </ng-container>
      </div>
    </div>
  `
})
export class PastRoutesComponent implements OnInit, OnChanges {
  @Input() public pastRoutes: RouteForDB[];

  public namePairs: Array<[string, string]> = [];

  constructor(readonly mapperiumService: MapperiumService) {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.pastRoutes.length > 5) {
      // Only show 5 past routes, even if it means we have to get the reverse geocodes agagin
      this.pastRoutes = this.pastRoutes.slice(0, 5);
      this.namePairs = [];
    }
    if (this.pastRoutes.length > this.namePairs.length) {
      const oPromises = [];
      const dPromises = [];
      let oNames = [];
      const diff = this.pastRoutes.length - this.namePairs.length;
      const startIdx = this.pastRoutes.length - diff;
      const relevantSlice = this.pastRoutes.slice(startIdx);
      relevantSlice.forEach((route: RouteForDB) => {
        oPromises.push(this.mapperiumService.getPlacename(route.origin));
        dPromises.push(this.mapperiumService.getPlacename(route.destination));
      });
      Promise.all(oPromises)
        .then((originNames: any[]) => {
          if (originNames.length) {
            oNames = originNames.map((oName: any) => oName.features[0].place_name || '');
          }
          return Promise.all(dPromises);

        })
        .then((destNames: string[]) => {
          if (destNames.length && oNames.length === destNames.length) {
            zip(oNames, destNames.map((dName: any) => dName.features[0].place_name || '')).forEach((pair: [string, string]) => {
              this.namePairs.push(pair);
            });
          }
        });
    }
  }

  formatRouteDate(dateString: string) {
    return moment.utc(dateString).format('MMMM Do YYYY, HH:mm:ss ZZ');
  }
}
