import { Component, OnInit } from '@angular/core';
import { WeatherAt } from './declarations';
import { RouteForDB } from './declarations';
import { NgxSpinnerService } from 'ngx-spinner';

// TODO: Add loading gif
@Component({
  selector: 'app-root',
  template: `
    <ngx-spinner
      bdColor="rgba(51, 51, 51, 0.8)"
      size="large"
      color="#fff"
      type="pacman">
      <p style="font-size: 30px; color: white;">Loading</p>
    </ngx-spinner>
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Wayfarer</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item" *ngIf="weatherList.length">
              <a class="nav-link js-scroll-trigger" href="#download">Directions</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#features">Past Routes</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="masthead">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-lg-12 m-auto">
            <mapperium
              (onMapUpdate)="onRouteChange($event)"
              (onFetchPastRoutes)="onFetchPastRoutes($event)"
              (onToggleSpinner)="onToggleSpinner($event)"
            ></mapperium>
          </div>
        </div>
      </div>
    </header>

    <section *ngIf="weatherList.length !== 0" class="download bg-primary" id="download">
      <div class="container">
        <div class="section-heading text-center">
          <h2>Weather along your route</h2>
          <hr>
        </div>
        <div class="row">
          <div class="col-md-12 mx-auto">
            <mapperium-directions [weatherList]="weatherList"></mapperium-directions>
          </div>
        </div>
      </div>
    </section>

    <section class="features" id="features">
      <div class="container">
        <div class="section-heading text-center">
          <h2>Past Routes</h2>
          <p class="text-muted">Check out where you've been before!</p>
          <hr>
        </div>
        <div class="row">
          <div class="col-md-12 mx-auto text-center">
            <past-routes [pastRoutes]="pastRoutes"></past-routes>
          </div>
        </div>
      </div>
    </section>

    <footer>
      <div class="container">
        <p>&copy; Dipack P Panjabi's CSE 586 Project 1 website 2018. All Rights Reserved.</p>
      </div>
    </footer>
  `
})
export class AppComponent implements OnInit {
  public weatherList: WeatherAt[];
  public pastRoutes: RouteForDB[];

  constructor(readonly spinnerService: NgxSpinnerService) {

  }

  ngOnInit() {
    this.weatherList = [];
    if (!this.pastRoutes) {
      this.pastRoutes = [];
    }
    this.spinnerService.show();
    setTimeout(() => {
      this.spinnerService.hide();
    }, 3000);
  }

  onRouteChange(weatherList: WeatherAt[]) {
    this.weatherList = weatherList;
  }

  onFetchPastRoutes(pastRoutes: RouteForDB[]) {
    this.pastRoutes = pastRoutes;
  }

  onToggleSpinner(state: boolean = false) {
    if (state === true) {
      this.spinnerService.show();
    } else {
      this.spinnerService.hide();
    }
  }
}
